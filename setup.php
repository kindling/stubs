<?php
define('KINDLING_STUBS_INSTALL_DIR', realpath(''));
define('KINDLING_STUBS_DIR', __DIR__ . '/stubs');

// Require all the things like cave people...
require_once __DIR__ . '/src/Installers/Installer.php';
require_once __DIR__ . '/src/Installers/EnvFile.php';
require_once __DIR__ . '/src/Installers/Config.php';
require_once __DIR__ . '/src/Installers/Git.php';
require_once __DIR__ . '/src/Installers/WordPress.php';

(new \Kindling\Stubs\Installers\EnvFile)->install();
(new \Kindling\Stubs\Installers\Config)->install();
(new \Kindling\Stubs\Installers\Git)->install();
(new \Kindling\Stubs\Installers\WordPress)->install();
