<?php

namespace Kindling\Stubs\Installers;

abstract class Installer
{
    abstract public function install();

    protected function stubExists($path = '')
    {
        return file_exists($this->stubPath($path));
    }

    protected function stubPath($path = '')
    {
        return KINDLING_STUBS_DIR . '/' . ltrim($path, '/');
    }

    protected function path($path = '')
    {
        return KINDLING_STUBS_INSTALL_DIR . '/' . ltrim($path, '/');
    }

    protected function exists($path = '')
    {
        return file_exists($this->path($path));
    }

    protected function stubPaths($path)
    {
        return $this->pathsFromDirectory(
            $directory = $this->stubPath("{$path}")
        );
    }

    protected function paths($path)
    {
        return $this->pathsFromDirectory(
            $directory = $this->path("{$path}")
        );
    }

    protected function pathsFromDirectory($directory)
    {
        $paths = glob("{$directory}/{,.}*", GLOB_BRACE);
        $paths = $paths ? $paths : [];
        $paths = array_map(function ($path) use ($directory) {
            $path = str_replace("{$directory}/", '', $path);

            return trim($path, '/');
        }, $paths);
        $paths = array_filter($paths, function ($path) {
            return !in_array($path, [
                '.',
                '..',
            ]);
        });

        return $paths;
    }

    protected function copyStubPaths($stubDirectory, $installDirectory = '')
    {
        foreach ($this->stubPaths($stubDirectory) as $path) {
            $this->copyStubPath($stubDirectory, $path, $installDirectory);
        }
    }

    protected function copyPaths($currentDirectory, $destinationDirectory = '')
    {
        foreach ($this->paths($currentDirectory) as $path) {
            $this->copyPath($currentDirectory, $path, $destinationDirectory);
        }
    }

    protected function copyPath($currentDirectory, $path, $destinationDirectory = '')
    {
        $current = "{$currentDirectory}/{$path}";
        if (!$this->exists($current)) {
            return;
        }

        $destination = $destinationDirectory ? "{$destinationDirectory}/{$path}" : $path;
        if ($this->exists($destination)) {
            return;
        }

        $currentPath = $this->path($current);
        $destinationPath = $this->path($destination);
        exec("cp -r {$currentPath} {$destinationPath}");
    }

    protected function copyStubPath($currentDirectory, $path, $destinationDirectory = '')
    {
        $current = "{$currentDirectory}/{$path}";
        if (!$this->stubExists($current)) {
            return;
        }

        $destination = $destinationDirectory ? "{$destinationDirectory}/{$path}" : $path;
        if ($this->exists($destination)) {
            return;
        }

        $currentPath = $this->stubPath($current);
        $destinationPath = $this->path($destination);
        exec("cp -r {$currentPath} {$destinationPath}");
    }
}
