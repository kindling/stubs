<?php

namespace Kindling\Stubs\Installers;

use Kindling\Stubs\Installers\Installer;

class EnvFile extends Installer
{
    public function install()
    {
        $stubName = '.env.example';
        $exampleName = '.env.example';
        $envName = '.env';

        if (!$this->stubExists($stubName)) {
            return;
        }

        if (!$this->exists($exampleName)) {
            copy($this->stubPath($stubName), $this->path($exampleName));
        }

        if (!$this->exists($envName)) {
            copy($this->path($exampleName), $this->path($envName));
        }
    }
}
