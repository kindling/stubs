<?php

namespace Kindling\Stubs\Installers;

use Kindling\Stubs\Installers\Installer;

class Git extends Installer
{
    public function install()
    {
        if (!$this->exists($installDirectory = '.git/hooks')) {
            return;
        }

        $this->copyStubPaths(
            $stubDirectory = 'git',
            $installDirectory
        );
    }
}
