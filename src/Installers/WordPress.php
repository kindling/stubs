<?php

namespace Kindling\Stubs\Installers;

use Kindling\Stubs\Installers\Installer;

class WordPress extends Installer
{
    protected $tempDirectory = 'wordpress';

    public function install()
    {
        if (!$this->exists($this->tempDirectory)) {
            return;
        }

        $this->copyPaths(
            $this->tempDirectory
        );

        $this->removeTempDirectory();
    }

    protected function removeTempDirectory()
    {
        $path = $this->path($this->tempDirectory);

        if (!file_exists($path)) {
            return;
        }

        if (!$path or $path === '/') {
            return;
        }

        exec("rm -rf {$path}");
    }
}
