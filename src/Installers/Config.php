<?php

namespace Kindling\Stubs\Installers;

use Kindling\Stubs\Installers\Installer;

class Config extends Installer
{
    public function install()
    {
        $this->copyStubPaths($stubDirectory = 'config');
    }
}
